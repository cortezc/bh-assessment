const generateAssessment = numQuestions => {
  let questions = []
  let answers = [{value: 0, title: "Not at all"}, {value: 1, title: "Several days"}]

  for (let i = 0; i < numQuestions; i++) {
    questions.push({ title: `do you like the number ${i}?`, key: `number_${i}`})
  }

  return {
    id: Math.random(),
    display_name: `Assessment-${Math.random()}`,
    title: `title-${Math.random()}`,
    questions,
    answers,
  }
}

export default generateAssessment
import React, { useState } from 'react'
import Assessments from './components/Assessments'
import { Button, ButtonGroup } from 'reactstrap';
import './stylesheets/App.css'

const apiUrl = "https://www.mocky.io/v2"
const userIds = {
  janeDoe: "5c7164773500007000e9e82a",
  frankSmith: "5c86be20340000e21389c26b"
}

function App() {
  const [assessments, setAssessments] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  const fetchAssessments = async user => {
    setIsLoading(true)

    await fetch(`${apiUrl}/${userIds[user]}`)
            .then(response => response.json())
            .then(assessments => { setIsLoading(false); setAssessments(assessments) })
  }

  return (
    <div className="app">
      <h2 className="app-header">Blueprint Assessments</h2>
      {assessments.length === 0 && isLoading && <div>Loading...</div>}
      {assessments.length === 0 && !isLoading &&
        <React.Fragment>
          <p>Welcome. Please select a user to continue.</p>
          <ButtonGroup vertical>
            <Button outline color="primary" onClick={() => fetchAssessments('frankSmith')}>Frank Smith</Button>
            <Button outline color="primary" onClick={() => fetchAssessments('janeDoe')}>Jane Doe</Button>
          </ButtonGroup>
        </React.Fragment>
      }
      {assessments.length > 0 && 
        <Assessments assessments={assessments} restartApp={() => setAssessments([])} />
      }
    </div>
  );
}

export default App
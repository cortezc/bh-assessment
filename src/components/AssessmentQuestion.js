import React from 'react'
import { Button, ButtonGroup } from 'reactstrap';

export default function AssessmentQuestion(props) {
  return <React.Fragment>
    <h4 className='question-title'>{props.question.title}</h4>
    <ButtonGroup vertical size="lg" className='answers'>
      { props.answers.map(answer => 
          <Button outline color="primary" key={answer.value} value={answer.value} onClick={props.onAnswerClick}>{answer.title}</Button>
      )}
    </ButtonGroup>
  </React.Fragment>
}
import React from 'react'
import { render } from 'react-testing-library'
import App from '../../App'

it('renders properly', () => {
  const { getByText } = render(<App />)
  expect(getByText(/Blueprint/)).toBeInTheDocument()
})
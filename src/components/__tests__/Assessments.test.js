import Assessments from '../Assessments'
import React from 'react';
import { render, fireEvent } from 'react-testing-library'
import generateAssessment from '../../testHelpers'

it('renders properly', () => {
  const assessments = [generateAssessment(2)]
  const { getByText } = render(<Assessments assessments={assessments} />)

  expect(getByText('Question 1 out of 2')).toBeInTheDocument()
  expect(getByText(assessments[0].display_name)).toBeInTheDocument()
  expect(getByText(assessments[0].title)).toBeInTheDocument()
  expect(getByText('do you like the number 0?')).toBeInTheDocument()
  expect(getByText('Not at all')).toBeInTheDocument()
  expect(getByText('Several days')).toBeInTheDocument()
})

it('advances to the next question when choosing an answer', () => {
  const assessments = [generateAssessment(2)]
  const { getByText } = render(<Assessments assessments={assessments} />)

  fireEvent.click(getByText('Several days'))
  expect(getByText('Question 2 out of 2')).toBeInTheDocument()
  expect(getByText('do you like the number 1?')).toBeInTheDocument()
})

describe('when there are multiple assessments', () => {
  it('renders properly', () => {
    const assessments = [generateAssessment(2), generateAssessment(2)]
    const { getByText } = render(<Assessments assessments={assessments} />)
    
    expect(getByText('Question 1 out of 4')).toBeInTheDocument()
    expect(getByText(assessments[0].display_name)).toBeInTheDocument()
    expect(getByText(assessments[0].title)).toBeInTheDocument()
    expect(getByText('do you like the number 0?')).toBeInTheDocument()
    expect(getByText('Not at all')).toBeInTheDocument()
    expect(getByText('Several days')).toBeInTheDocument()
  })

  it('advances to the next assessment when choosing an answer for the last question', () => {
    const assessments = [generateAssessment(1), generateAssessment(1)]
    const { getByText } = render(<Assessments assessments={assessments} />)

    fireEvent.click(getByText('Not at all'))
    expect(getByText(assessments[1].display_name)).toBeInTheDocument()
    expect(getByText(assessments[1].title)).toBeInTheDocument()
    expect(getByText('Question 2 out of 2')).toBeInTheDocument()
  })

  it('goes back to the previous assessment when clicking back', () => {
    const assessments = [generateAssessment(1), generateAssessment(1)]
    const { getByText } = render(<Assessments assessments={assessments} />)

    fireEvent.click(getByText('Not at all'))
    fireEvent.click(getByText('Previous Question'))
    expect(getByText(assessments[0].display_name)).toBeInTheDocument()
    expect(getByText(assessments[0].title)).toBeInTheDocument()
  })
})

it('outputs results at the end of all assessments', () => {
  const assessments = [generateAssessment(1)]
  const { getByText } = render(<Assessments assessments={assessments} />)

  fireEvent.click(getByText('Not at all'))
  const expectedResults = { 
    assessments: [
      { id: assessments[0].id, answers: [{key: 'number_0', value: "0"}] }
    ]
  }

  expect(getByText(JSON.stringify(expectedResults))).toBeInTheDocument()
})
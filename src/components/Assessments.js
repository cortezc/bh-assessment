import React, { useState } from 'react'
import { findIndex } from 'lodash'
import AssessmentQuestion from './AssessmentQuestion'
import { Button, Progress } from 'reactstrap';

export default function Assessments(props) {
  const { assessments } = props
  const initializeAnswers = () => {
    return assessments.map(assessment => ({ "id":  assessment.id, answers: [] }))
  }

  const [allAssessmentsCompleted, setAllAssessmentsCompleted] = useState(false)
  const [allAnswers, setAllAnswers] = useState(initializeAnswers)
  const [questionIndex, setQuestionIndex] = useState(0)  
  const [assessmentIndex, setAssessmentIndex] = useState(0)

  const currentAssessment = assessments[assessmentIndex]
  const currentQuestion = currentAssessment.questions[questionIndex]

  const handleAnswerClick = event => {
    updateCurrentAnswer(event.target.value)
    goToNextQuestion()
  }

  const updateCurrentAnswer = newValue => {
    const currentAssessmentAnswersIndex = findIndex(allAnswers, { "id": currentAssessment.id })
    const currentAssessmentAnswers = allAnswers[currentAssessmentAnswersIndex].answers
    const currentAnswerIndex = findIndex(currentAssessmentAnswers, { "key": currentQuestion.key })
    const newAnswer = { "key": currentQuestion.key, "value": newValue }

    if (currentAnswerIndex >= 0) {
      currentAssessmentAnswers.splice(currentAnswerIndex, 1, newAnswer)
    } else {
      currentAssessmentAnswers.push(newAnswer)
    }

    allAnswers[currentAssessmentAnswersIndex].answers = currentAssessmentAnswers
    setAllAnswers(allAnswers)
  }

  const goToNextQuestion = () => {
    if (hasMoreQuestions()) {
      setQuestionIndex(questionIndex + 1)
    } else if (hasMoreAssessments()){
      setAssessmentIndex(assessmentIndex + 1)
      setQuestionIndex(0)
    } else {
      setAllAssessmentsCompleted(true)
      console.log({"assessments": allAnswers})
    }
  }

  const goToPreviousQuestion = () => {
    if (questionIndex > 0) {
      setQuestionIndex(questionIndex - 1)
    } else {
      const newAssessmentIndex = assessmentIndex - 1
      setAssessmentIndex(newAssessmentIndex)
      setQuestionIndex(assessments[newAssessmentIndex].questions.length - 1)
    }
  }

  const hasMoreQuestions = () => {
    return questionIndex < currentAssessment.questions.length - 1
  }

  const hasMoreAssessments = () => {
    return assessmentIndex < assessments.length - 1   
  }

  const getQuestionNumber = () => {
    let base = 0

    for (let i = 0; i < assessmentIndex; i++) {
      base += assessments[i].questions.length
    }

    return base + questionIndex + 1
  }

  const getTotalQuestions = () =>
    assessments.reduce((total, assessment) => assessment.questions.length + total, 0)

  const getProgressValue = () => (getQuestionNumber() / getTotalQuestions()) * 100

  return <React.Fragment>
    {allAssessmentsCompleted && <React.Fragment>
      <div>{JSON.stringify({"assessments": allAnswers})}</div><p>You can also see the console for results</p>
      <button onClick={props.restartApp}>Restart</button>
    </React.Fragment> }

    {!allAssessmentsCompleted && <React.Fragment>
      <Progress value={getProgressValue()} />

      <div className='assessment-name'>{currentAssessment.display_name}</div>
      <div className='question-number'><span>Question {getQuestionNumber()} out of {getTotalQuestions()}</span></div>

      <div className='assessment-container'>
        <h4 className='assessment-title'>{currentAssessment.title}</h4>
        <AssessmentQuestion question={currentQuestion} answers={currentAssessment.answers} onAnswerClick={handleAnswerClick} />
      </div>

      <div>
        {(assessmentIndex > 0 || questionIndex > 0) &&
          <Button outline color='secondary' className='previous-question' onClick={goToPreviousQuestion}>Previous Question</Button>
        }
      </div>
    </React.Fragment>}
  </React.Fragment>
}
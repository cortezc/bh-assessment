## Link to application: https://salty-dusk-47986.herokuapp.com/
# The problem
To help facilitate measurement-based care, patient assessments can be administered remotely by clinicians. The problem presented is the creation of a user-friendly, adaptable interface that makes taking the assessment as simple and comfortable as possible for the patient, while collecting key data sets for a wide variety of assessments.
# The solution
The solution presented here is a mobile-friendly web application which allows patients to take assessments whenever, wherever, on any device they choose. If a patient has multiple assessments to complete, they are presented one after another so that the user experiences a seamless interaction. Meanwhile, the answers to the assessments are stored and ultimately presented at the end of all assessments.
# Technical choices
When considering which technology to use to build this tool, I thought a lot about the end-users: mental health patients with all kinds of backgrounds. Even though the current Blueprint application is a native mobile app, I thought about the users who might not be as savvy and have the knowledge or desire to download apps on their phone, let alone those who might not even have one. To this end, I thought a mobile-friendly web application would seem presentable to more users and also eliminate the restriction of operating on a mobile phone. This way, the assessments could also be taken in any browser on a desktop computer.

With that in mind, I did want the code to be portable to a mobile app so that the coverage of use cases was as wide as possible. To that end, I decided to implement the application using ReactJS. React has proven to be an extremely reliable and browser-friendly framework with a lot of support, and with the emergence of frameworks like React-Native, which allow native apps to be built for iOS and Android using a single codebase, it's becoming easier to port React code written for the browser onto a mobile app. I also used `create-react-app` to skip unnecessary configuration.

In addition, I wanted to try out one of the new features of React which has gotten a lot of buzz lately: React Hooks. Hooks are getting a lot of attention for simplifying the component lifecycle management and sharing stateful logic across components. In this code, you will see that there are no traditional `Class` components and instead the entire app consists of just three components using hooks to manage state.
# Trade-offs
Having to ramp up on React Hooks and make the interface as flexible as possible for multiple assessments, I had to, unfortunately, cut some corners on the interface design. To mitigate this as much as possible, I used a lightweight UI component library called `reactstrap` which allows me to use Bootstrap components in a JSX-friendly way. This also means there was little work to make the app mobile-friendly from the start.
# Improvements
If I had more time, I would like to implement a back-end to manage the serving of assessments and saving of results. In addition, this would make authentication much easier and allow for a more robust, secure portal for the patient. I would also like to implement a `react-router`, linter, and flesh out the CSS styles some more.

Apart from data, I would like to use CSS animations to make the interface even more friendly and seamless, perhaps by adding a fade-in and fade-out transition between each question.
# Personal app
I've been working on a playlist generation application in Rails which collects user's favorite tracks and artists from Spotify, allows them to make groups with other users, and then creates a playlist based on everyone's interests. I've been working with a designer and teaching her to code HTML and CSS as well. The application can be found here: http://musicwelike.herokuapp.com
# LinkedIn
https://www.linkedin.com/in/ccortez3243/